package com.iserveu.dmtindus.utils;

import java.util.zip.CRC32;

public class Utility {

	public static String getAgentValue(String username, String key) {
        String agentName = null;
        try {
            String originalString = key.trim() + username.trim();
            CRC32 crc = new CRC32();
            crc.update(originalString.getBytes());
            agentName = Long.toString(crc.getValue());
        } catch (Exception e) {
            agentName = null;
        }
        return agentName;
    }
	
}
