
package com.iserveu.dmtindus.services.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.iserveu.dmtindus.requests.IndusBankAPIRequest;
import com.iserveu.dmtindus.responses.IndusBankAPIResponse;
import com.iserveu.dmtindus.services.IndusAPIService;

@Service
public class IndusAPIServiceImpl implements IndusAPIService {
	
	static final Logger LOGGER = Logger.getLogger(IndusAPIServiceImpl.class);
	
	@Autowired
	private Environment environment;

	@Override
	public IndusBankAPIResponse consumeIndusTransactionAPI(IndusBankAPIRequest request) {

		IndusBankAPIResponse finalResponse = new IndusBankAPIResponse();

		Gson gson = new Gson();
		LOGGER.info("IBL Bank API Request >>>>>>>> " + gson.toJson(request) 
					+ ", URL Consume >>>>>>>>>" + environment.getProperty("IBL_TRANSACTION_URL"));
		
		HttpEntity<String> requestEntity = new HttpEntity<>(gson.toJson(request), getHttpHeader());

		try {
			
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(environment.getProperty("IBL_TRANSACTION_URL"),
																			requestEntity, 
																			String.class);
			
			finalResponse = gson.fromJson(response.getBody(), IndusBankAPIResponse.class);

		} catch (Exception e) {
			
			finalResponse.setStatusCode("1");
			finalResponse.setStatusDesc("Exception occured at IBL bank API saying - " + e.getMessage());
			finalResponse.setCustomerRefNo(request.getCustomerRefNumber());
		}
		
		LOGGER.info("IBL Bank Response >>>>>>>>>> " + finalResponse.toString());
		return finalResponse;
		
	}
	
	private HttpHeaders getHttpHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("X-IBM-Client-Id", "98228386-ca8e-4e4c-a78c-af639d95d3f0");
		headers.add("X-IBM-Client-Secret", "eL3qW7xV6cW7iO2kW4mH4rP2kQ5xQ5oU7jU7jQ6wI1hP8wK4rM");
		return headers;
	}

	@Override
	public IndusBankAPIResponse consumeIndusBeneVerificationAPI(IndusBankAPIRequest request) {
		
		IndusBankAPIResponse indusBankAPIResponse = new IndusBankAPIResponse();

		Gson gson = new Gson();
		LOGGER.info("INDUS Bank API Request >>>>>>>> " + gson.toJson(request) 
					+ ", URL Consume >>>>>>>>>" + environment.getProperty("IBL_TRANSACTION_URL"));
		
		HttpEntity<String> requestEntity = new HttpEntity<>(gson.toJson(request), getHttpHeader());

		try {
			
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.postForEntity(environment.getProperty("IBL_TRANSACTION_URL"),
																			requestEntity, 
																			String.class);
			
			indusBankAPIResponse = gson.fromJson(response.getBody(), IndusBankAPIResponse.class);

		} catch (Exception e) {
			
			indusBankAPIResponse.setStatusCode("1");
			indusBankAPIResponse.setStatusDesc("Exception occured at IBL bank API saying - " + e.getMessage());
			indusBankAPIResponse.setCustomerRefNo(request.getCustomerRefNumber());
		}
		
		LOGGER.info("IBL Bank Response >>>>>>>>>> " + indusBankAPIResponse.toString());
		return indusBankAPIResponse;
		
	}

	@Override
	public IndusBankAPIResponse consumeIBLTransactionStatusEnquiryAPI(String clientUniqueId) {
		
		IndusBankAPIResponse response = new IndusBankAPIResponse();
		
		Map<String, String> request = new LinkedHashMap<>();
		request.put("Customerid", "38739475");
		request.put("CustomerRefNumber", clientUniqueId);
		
		Gson gson = new Gson();
		LOGGER.info("IBL Bank API Request >>>>>>>>>> " + gson.toJson(request) 
						+ ", URL Consumed >>>>>>>>> " + environment.getProperty("IBL_STATUS_ENQUIRY_URL"));
		
		HttpEntity<String> requestEntity = new HttpEntity<>(gson.toJson(request), getHttpHeader());

		try {
			
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> apiResponse = restTemplate.postForEntity(environment.getProperty("IBL_STATUS_ENQUIRY_URL"),
																			requestEntity, 
																			String.class);
			
			response = gson.fromJson(apiResponse.getBody(), IndusBankAPIResponse.class);

		} catch (Exception e) {
			
			response.setStatusCode("1");
			response.setStatusDesc("Exception occured at IBL Status Enquiry API, saying - " + e.getMessage());
			response.setCustomerRefNo(clientUniqueId);

		}
		
		LOGGER.info("IBL Bank API Response >>>>>>>>>>>" + response);
		return response;
		
		
	}

}
