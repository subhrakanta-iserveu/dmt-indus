package com.iserveu.dmtindus.services.impl;

import java.util.function.Function;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.iserveu.dmtindus.constants.DMTConstants;
import com.iserveu.dmtindus.requests.IndusBankAPIRequest;
import com.iserveu.dmtindus.requests.IndusEndpointRequest;
import com.iserveu.dmtindus.responses.IndusBankAPIResponse;
import com.iserveu.dmtindus.responses.IndusEndpointResponse;
import com.iserveu.dmtindus.services.IndusService;
import com.iserveu.dmtindus.services.IndusAPIService;
import com.iserveu.dmtindus.utils.Utility;

@Service
public class IndusServiceImpl implements IndusService {
	
	static final Logger LOGGER = Logger.getLogger(IndusServiceImpl.class);
	
	@Autowired
	private IndusAPIService indusAPIService;
	
	@Value("${INDUS_AGENT_KEY}")
	private String INDUS_AGENT_KEY;

	@Override
	public IndusEndpointResponse doTransaction(IndusEndpointRequest request) {

		LOGGER.info("INDUS transaction Service reuqest >>>>>>> " + request);

		IndusBankAPIRequest indusAPIRequest = toIndusTransactionAPIRequest.apply(request);
		IndusBankAPIResponse indusAPIResponse = indusAPIService.consumeIndusTransactionAPI(indusAPIRequest);

		if (indusAPIResponse.getStatusCode().equalsIgnoreCase("R000")) {

			return setIndusAPIResponse(indusAPIResponse, 0, DMTConstants.STATUS_SUCCESS);

		} else if (indusAPIResponse.getStatusCode().equalsIgnoreCase("R002")
				|| indusAPIResponse.getStatusCode().equalsIgnoreCase("R009")) {

			return setIndusAPIResponse(indusAPIResponse, -1, DMTConstants.STATUS_FAILED);

		} else if (indusAPIResponse.getStatusCode().equalsIgnoreCase("R003")
				|| indusAPIResponse.getStatusCode().equalsIgnoreCase("R005")
				|| indusAPIResponse.getStatusCode().equalsIgnoreCase("R008")
				|| indusAPIResponse.getStatusCode().equalsIgnoreCase("R020")
				|| indusAPIResponse.getStatusCode().equalsIgnoreCase("1")) {

			return setIndusAPIResponse(indusAPIResponse, 1, DMTConstants.STATUS_INPROGRESS);

		} else {

			return setIndusAPIResponse(indusAPIResponse, 1, DMTConstants.STATUS_UNKNOWN);

		}
	}
	
	Function<IndusEndpointRequest, IndusBankAPIRequest> toIndusTransactionAPIRequest = p -> {

		IndusBankAPIRequest indusAPIRequest = new IndusBankAPIRequest();
		indusAPIRequest.setTransactionType(p.getTransactionMode());
		indusAPIRequest.setCustomerRefNumber(p.getClientUniqueID().toString());
		indusAPIRequest.setTransaction("1");
		indusAPIRequest.setIsAsync("0");
		indusAPIRequest.setDebitAccountNo("201003405575");
		indusAPIRequest.setAmount(p.getAmount());
		indusAPIRequest.setBeneficiaryName(p.getBeneName());
		indusAPIRequest.setCreditAccountNumber(p.getBeneAccountNo());
		indusAPIRequest.setBeneficiaryBankIFSCCode(p.getBeneIFSCCode());
		indusAPIRequest.setBeneficiaryBranch(p.getBeneName());
		indusAPIRequest.setBeneficiaryBank(p.getBankName() == null ? "" : p.getBankName());
		indusAPIRequest.setBeneficiaryMobileNumber("");
		indusAPIRequest.setBeneficiaryEmailID("");
		indusAPIRequest.setRemitterName(p.getCustomerMobileNo());
		indusAPIRequest.setRemitterMobileNo(p.getCustomerMobileNo());
		indusAPIRequest.setRemitterAddress1("");
		indusAPIRequest.setRemitterAddress2("");
		indusAPIRequest.setRemitterPin(p.getPincode() == null ? "753011" : p.getPincode());
		indusAPIRequest.setRemitterKYC("0");
		indusAPIRequest.setCustomerid("38739475");
		indusAPIRequest.setCheckerID("DEBIPRASAD");
		indusAPIRequest.setAgentID(Utility.getAgentValue(p.getUsername(), this.INDUS_AGENT_KEY) != null
				? Utility.getAgentValue(p.getUsername(), this.INDUS_AGENT_KEY)
				: p.getUsername());
		indusAPIRequest.setReserve1("");
		indusAPIRequest.setReserve2("");
		indusAPIRequest.setReserve3("");
		indusAPIRequest.setReserve4("");
		indusAPIRequest.setReserve5("");
		return indusAPIRequest;

	};

	private IndusEndpointResponse setIndusAPIResponse(IndusBankAPIResponse apiResponse, 
														int statusCode,
														String status) {

		IndusEndpointResponse response = new IndusEndpointResponse();

		response.setStatusCode(statusCode);
		response.setStatus(status);
		response.setStatusDesc(apiResponse.getStatusDesc() != null ? apiResponse.getStatusDesc() : "");
		response.setAmount(apiResponse.getAmount() != null ? apiResponse.getAmount() : "");
		response.setTransactionType(apiResponse.getTranType() != null ? apiResponse.getTranType() : "");
		response.setBankRefNo(apiResponse.getIBLRefNo() != null ? apiResponse.getIBLRefNo() : "");
		
		String customerRefNo = apiResponse.getCustomerRefNo() != null 
					? apiResponse.getCustomerRefNo() : apiResponse.getCustomerRefNum() != null 
						? apiResponse.getCustomerRefNum() : "";
		response.setClientUniqueId(customerRefNo);

		String rrn = apiResponse.getUTRNo() != null 
				? apiResponse.getUTRNo() : apiResponse.getUTR() != null 
					? apiResponse.getUTR() : "";
		response.setRrn(rrn);
		
		String beneName = apiResponse.getIMPSBeneName() != null 
				? apiResponse.getIMPSBeneName() : apiResponse.getBeneficiaryName() != null 
					? apiResponse.getBeneficiaryName() : "";
		response.setBeneName(beneName);

		return response;

	}

	@Override
	public IndusEndpointResponse doBeneficiaryVerification(IndusEndpointRequest request) {

		LOGGER.info("INDUS transaction Service reuqest >>>>>>> " + request);

		IndusBankAPIRequest indusBankAPIRequest = toIBLBankBeneVerificationAPIRequest.apply(request);
		IndusBankAPIResponse apiResponse = indusAPIService.consumeIndusBeneVerificationAPI(indusBankAPIRequest);
		
		if (apiResponse.getStatusCode().equalsIgnoreCase("R000")) {

			return setIndusAPIResponse(apiResponse, 0, DMTConstants.STATUS_SUCCESS);

		} else if (apiResponse.getStatusCode().equalsIgnoreCase("R002")
					|| apiResponse.getStatusCode().equalsIgnoreCase("R009")) {

			return setIndusAPIResponse(apiResponse, -1, DMTConstants.STATUS_FAILED);

		} else if (apiResponse.getStatusCode().equalsIgnoreCase("R003")
					|| apiResponse.getStatusCode().equalsIgnoreCase("R005")
					|| apiResponse.getStatusCode().equalsIgnoreCase("R008")
					|| apiResponse.getStatusCode().equalsIgnoreCase("R020")
					|| apiResponse.getStatusCode().equalsIgnoreCase("1")) {

			return setIndusAPIResponse(apiResponse, -1, DMTConstants.STATUS_INPROGRESS);

		} else {

			return setIndusAPIResponse(apiResponse, 1, DMTConstants.STATUS_UNKNOWN);

		}
		
	}
	
	Function<IndusEndpointRequest, IndusBankAPIRequest> toIBLBankBeneVerificationAPIRequest = p -> {

		IndusBankAPIRequest indusBankAPIRequest = new IndusBankAPIRequest();
		indusBankAPIRequest.setTransactionType(p.getTransactionMode());
		indusBankAPIRequest.setCustomerRefNumber(p.getClientUniqueID().toString());
		indusBankAPIRequest.setTransaction("0");
		indusBankAPIRequest.setIsAsync("0");
		indusBankAPIRequest.setDebitAccountNo("201003405575");
		indusBankAPIRequest.setAmount(p.getAmount());
		indusBankAPIRequest.setBeneficiaryName(p.getBeneName());
		indusBankAPIRequest.setCreditAccountNumber(p.getBeneAccountNo());
		indusBankAPIRequest.setBeneficiaryBankIFSCCode(p.getBeneIFSCCode());
		indusBankAPIRequest.setBeneficiaryBranch(p.getBeneName());
		indusBankAPIRequest.setBeneficiaryBank(p.getBankName() == null ? "" : p.getBankName());
		indusBankAPIRequest.setBeneficiaryMobileNumber("");
		indusBankAPIRequest.setBeneficiaryEmailID("");
		indusBankAPIRequest.setRemitterName(p.getCustomerMobileNo());
		indusBankAPIRequest.setRemitterMobileNo(p.getCustomerMobileNo());
		indusBankAPIRequest.setRemitterAddress1("");
		indusBankAPIRequest.setRemitterAddress2("");
		indusBankAPIRequest.setRemitterPin(p.getPincode() == null ? "753011" : p.getPincode());
		indusBankAPIRequest.setRemitterKYC("0");
		indusBankAPIRequest.setCustomerid("38739475");
		indusBankAPIRequest.setCheckerID("DEBIPRASAD");
		indusBankAPIRequest.setAgentID(Utility.getAgentValue(p.getUsername(), this.INDUS_AGENT_KEY) != null
				? Utility.getAgentValue(p.getUsername(), this.INDUS_AGENT_KEY)
				: p.getUsername());
		indusBankAPIRequest.setReserve1("");
		indusBankAPIRequest.setReserve2("");
		indusBankAPIRequest.setReserve3("");
		indusBankAPIRequest.setReserve4("");
		indusBankAPIRequest.setReserve5("");
		return indusBankAPIRequest;

	};

	@Override
	public IndusEndpointResponse doStatusEnquiry(String clientUniqueId) {
		
		LOGGER.info("INDUS status enquiry Service reuqest >>>>>>> " + clientUniqueId);
		
		IndusBankAPIResponse indusBankAPIResponse 
			= indusAPIService.consumeIBLTransactionStatusEnquiryAPI(clientUniqueId);
		
		if (indusBankAPIResponse.getStatusCode().equalsIgnoreCase("S")) {
			
			LOGGER.info(">>>>>>>>> Success response got from Bank with status code : " + indusBankAPIResponse.getStatusCode());
			return setIndusAPIResponse(indusBankAPIResponse, 0, DMTConstants.STATUS_SUCCESS);

		} else if ((indusBankAPIResponse.getStatusCode().equalsIgnoreCase("J"))
				|| (indusBankAPIResponse.getStatusCode().equalsIgnoreCase("R"))
				|| (indusBankAPIResponse.getStatusCode().equalsIgnoreCase("F"))) {
			
			LOGGER.info(">>>>>>>>> Failed response got from Bank with status code : " + indusBankAPIResponse.getStatusCode());
			return setIndusAPIResponse(indusBankAPIResponse, -1, DMTConstants.STATUS_FAILED);
			
		} else if ((indusBankAPIResponse.getStatusCode().equalsIgnoreCase("SP"))
				|| (indusBankAPIResponse.getStatusCode().equalsIgnoreCase("R019"))
				|| (indusBankAPIResponse.getStatusCode().equalsIgnoreCase("SH"))
				|| (indusBankAPIResponse.getStatusCode().equalsIgnoreCase("R009"))) {
			
			LOGGER.info(">>>>>>>>> Pending response got from Bank with status code : " + indusBankAPIResponse.getStatusCode());
			return setIndusAPIResponse(indusBankAPIResponse, 1, DMTConstants.STATUS_INPROGRESS);
			
		} else {
			
			LOGGER.info(">>>>>>>>> Unknown response got from Bank with status code : " + indusBankAPIResponse.getStatusCode());
			return setIndusAPIResponse(indusBankAPIResponse, 1, DMTConstants.STATUS_UNKNOWN);
			
		}
		
	}

}
