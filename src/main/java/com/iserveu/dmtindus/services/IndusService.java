package com.iserveu.dmtindus.services;

import com.iserveu.dmtindus.requests.IndusEndpointRequest;
import com.iserveu.dmtindus.responses.IndusEndpointResponse;

public interface IndusService {

	IndusEndpointResponse doTransaction(IndusEndpointRequest request);

	IndusEndpointResponse doBeneficiaryVerification(IndusEndpointRequest request);

	IndusEndpointResponse doStatusEnquiry(String clientUniqueId);

}
