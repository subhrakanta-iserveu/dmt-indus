package com.iserveu.dmtindus.services;

import com.iserveu.dmtindus.requests.IndusBankAPIRequest;
import com.iserveu.dmtindus.responses.IndusBankAPIResponse;

public interface IndusAPIService {

	IndusBankAPIResponse consumeIndusTransactionAPI(IndusBankAPIRequest indusBankAPIRequest);

	IndusBankAPIResponse consumeIndusBeneVerificationAPI(IndusBankAPIRequest indusBankAPIRequest);

	IndusBankAPIResponse consumeIBLTransactionStatusEnquiryAPI(String clientUniqueId);


}
