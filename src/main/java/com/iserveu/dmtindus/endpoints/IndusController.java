package com.iserveu.dmtindus.endpoints;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.iserveu.dmtindus.requests.IndusEndpointRequest;
import com.iserveu.dmtindus.responses.StatusResponse;
import com.iserveu.dmtindus.responses.IndusEndpointResponse;
import com.iserveu.dmtindus.services.IndusService;

@RestController
@CrossOrigin
public class IndusController {
	
	static final Logger LOGGER = Logger.getLogger(IndusController.class);
	
	@Autowired
	private IndusService indusService;
	
	@GetMapping("/")
	public ResponseEntity<?> welcome() {
		return new ResponseEntity<>("Welcome to DMT-INDUS API application", HttpStatus.OK);
	}
	
	@PostMapping("/indus/transaction")
	public ResponseEntity<?> doTransaction(@Valid @RequestBody IndusEndpointRequest request,
												BindingResult result) {
		
		if (result.hasErrors()) {
			
			LOGGER.error("Validation Error >>>>>>>> " + result.getFieldError().getDefaultMessage());
			return new ResponseEntity<>(new StatusResponse(-1, result.getFieldError().getDefaultMessage()), 
											HttpStatus.BAD_REQUEST);
		}
		
		LOGGER.info("INDUS transaction API reuqest >>>>>>> " + request);
		
		try {
			
			IndusEndpointResponse response = indusService.doTransaction(request);
			
			LOGGER.info("INDUS transaction response >>>>>>> " + response);
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		} catch(Exception exception) {
			
			LOGGER.info(">>>>>>>> Custom exception, saying - " + exception.getMessage());
			return new ResponseEntity<>(new StatusResponse(-1, "Custom exception, saying - " + exception.getMessage()), 
											HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
	}
	
	@PostMapping("/indus/verify-bene")
	public ResponseEntity<?> doBeneficiaryVerification(@RequestBody @Valid IndusEndpointRequest request,
															BindingResult result) {
		
		if (result.hasErrors()) {
			
			LOGGER.error("Validation Error >>>>>>>>> " + result.getFieldError().getDefaultMessage());
			return new ResponseEntity<>(new StatusResponse(-1, result.getFieldError().getDefaultMessage()), 
											HttpStatus.BAD_REQUEST);
		}
		
		LOGGER.info("INDUS beneficiary verification API reuqest >>>>>>> " + request);
		
		try {
			
			IndusEndpointResponse response = indusService.doBeneficiaryVerification(request);
			LOGGER.info("INDUS beneficiary verification response >>>>>>> " + response);
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		} catch(Exception exception) {
			
			LOGGER.info(">>>>>>>> Custom exception, saying - " + exception.getMessage());
			return new ResponseEntity<>(new StatusResponse(-1, "Custom exception, saying - " + exception.getMessage()), 
											HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
	}
	
	@PostMapping("/indus/status-enquiry/{clientUniqueId}")
	public ResponseEntity<?> doStatusEnquiry(@PathVariable String clientUniqueId) {
		
		LOGGER.info("INDUS status enquiry API reuqest >>>>>>> " + clientUniqueId);
		
		try {
			
			IndusEndpointResponse response = indusService.doStatusEnquiry(clientUniqueId);
			LOGGER.info("INDUS status enquiry response >>>>>>> " + response);
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		} catch(Exception exception) {
			
			LOGGER.info(">>>>>>>> Custom exception, saying - " + exception.getMessage());
			return new ResponseEntity<>(new StatusResponse(-1, "Custom exception, saying - " + exception.getMessage()), 
											HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
	}

}
