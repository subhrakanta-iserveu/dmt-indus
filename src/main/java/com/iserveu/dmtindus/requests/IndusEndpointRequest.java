package com.iserveu.dmtindus.requests;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndusEndpointRequest {

	@NotNull(message = "clientUniqueID must not be NULL")
	private Long clientUniqueID;
	
	@NotNull(message = "customerMobileNo must not be NULL")
	private String customerMobileNo;
	
	@NotNull(message = "beneIFSCCode must not be NULL")
	private String beneIFSCCode;
	
	@NotNull(message = "beneAccountNo must not be NULL")
	private String beneAccountNo;
	
	@NotNull(message = "beneName must not be NULL")
	private String beneName;
	
	@NotNull(message = "amount must not be NULL")
	private String amount;
	
	@NotNull(message = "customerName must not be NULL")
	private String customerName;
	
	@NotNull(message = "transactionMode must not be NULL")
	private String transactionMode;
	
	@NotNull(message = "bankName must not be NULL")
	private String bankName;
	
	@NotNull(message = "username must not be NULL")
	private String username;
	
	@NotNull(message = "pincode must not be NULL")
    private String pincode;
	
}
