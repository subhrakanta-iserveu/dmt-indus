package com.iserveu.dmtindus.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndusBankAPIRequest {

	private String TransactionType;
	private String CustomerRefNumber;
	private String Transaction;
	private String isAsync;
	private String DebitAccountNo;
	private String Amount;
	private String BeneficiaryName;
	private String CreditAccountNumber;
	private String BeneficiaryBankIFSCCode;
	private String BeneficiaryBranch;
	private String BeneficiaryBank;
	private String BeneficiaryMobileNumber;
	private String BeneficiaryEmailID;
	private String RemitterName;
	private String RemitterMobileNo;
	private String RemitterAddress1;
	private String RemitterAddress2;
	private String RemitterPin;
	private String RemitterKYC;
	private String Customerid;
	private String CheckerID;
	private String AgentID;
	private String Reserve1;
	private String Reserve2;
	private String Reserve3;
	private String Reserve4;
	private String Reserve5;
	
}
