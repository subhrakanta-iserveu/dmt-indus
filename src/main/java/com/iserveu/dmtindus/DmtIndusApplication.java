package com.iserveu.dmtindus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DmtIndusApplication {

	public static void main(String[] args) {
		SpringApplication.run(DmtIndusApplication.class, args);
	}

}
