package com.iserveu.dmtindus.constants;

public class DMTConstants {

	public static final String STATUS_INPROGRESS = "INPROGRESS";
	public static final String STATUS_SUCCESS = "SUCCESS";
	public static final String STATUS_FAILED = "FAILED";
	public static final String STATUS_UNKNOWN = "Unknown response";

	public static final String BANK_INDUS = "INDUS";
	public static final String BANK_FINO = "FINO";
	public static final String BANK_AIRTEL = "AIRTEL";
	
	
}
