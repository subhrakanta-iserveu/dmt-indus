package com.iserveu.dmtindus.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatusResponse {

	private Integer statusCode;
	
	private String statusDesc;
	
}
