package com.iserveu.dmtindus.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndusBankAPIResponse {

	private String IBLRefNo;
	private String CustomerRefNo;//For Transaction and Bene Verification
	private String CustomerRefNum;//For Status Enquiry
	private String TranType;
	private String Amount;
	private String StatusCode;
	private String StatusDesc;
	private String UTRNo;//For Transaction and Bene Verification
	private String UTR;//For Status Enquiry
	private String IMPSBeneName;//For Transaction and Bene Verification
	private String PaymentDate;
	private String BeneficiaryName;//For Status Enquiry

}
