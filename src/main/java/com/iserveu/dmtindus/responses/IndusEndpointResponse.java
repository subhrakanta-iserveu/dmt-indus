package com.iserveu.dmtindus.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndusEndpointResponse {
	
	private int statusCode;
	private String status;
	private String statusDesc;
	private String clientUniqueId;
	private String rrn;
	private String beneName;
	private String amount;
	private String transactionType;
	private String bankRefNo;
	
}
